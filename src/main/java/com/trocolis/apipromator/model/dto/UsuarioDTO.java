package com.trocolis.apipromator.model.dto;

import com.trocolis.apipromator.model.entity.Usuario;
import com.trocolis.apipromator.model.enums.TipoUsuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@Data
@AllArgsConstructor
public class UsuarioDTO {

    private String username;
    private String password;
    private TipoUsuario role;

    public UserDetails getUserDetails() {
        return User.builder()
                .username(this.username)
                .password(this.password)
                .roles(this.role.toString())
                .build();
    }

}

package com.trocolis.apipromator.model.dto;

import lombok.Data;

@Data
public class CredenciaisDTO {
    private String username;
    private String password;
}

package com.trocolis.apipromator.model.enums;

public enum TipoUsuario {
    ADMIN, LOJA, CLIENTE, ENTREGADOR, ANONIMO;
}

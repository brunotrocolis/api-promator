package com.trocolis.apipromator.model.enums;

public enum SituacaoUsuario {
    ATIVO, INATIVO, BLOQUEADO, EXCLUIDO;
}

package com.trocolis.apipromator.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.trocolis.apipromator.model.enums.SituacaoUsuario;
import com.trocolis.apipromator.model.enums.TipoUsuario;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "CAD_USUARIO")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_USUARIO")
    private Long id;

    @Column(name = "NO_USUARIO", unique = true)
    private String nome;

    @Column(name = "SE_USUARIO")
    private String senha;

    @Column(name = "TP_USUARIO")
    @Enumerated(EnumType.STRING)
    private TipoUsuario tipo;

    @Column(name = "ST_USUARIO")
    @Enumerated(EnumType.STRING)
    private SituacaoUsuario situacao;
}

package com.trocolis.apipromator.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CAD_ENDERECO")
public class Endereco {

    @Id
    @Column(name = "ID_ENDERECO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NU_ENDERECO")
    private Long numero;

    @Column(name = "TX_DESCRICAO")
    private String descricao;

    @Column(name = "NU_LATITUDE")
    private Double latitude;

    @Column(name = "NU_LONGITUDE")
    private Double longitude;

}

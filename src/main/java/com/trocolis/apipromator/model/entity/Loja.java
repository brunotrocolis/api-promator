package com.trocolis.apipromator.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "CAD_LOJA")
public class Loja {
    @Id
    @Column(name = "ID_LOJA")
    private Long id;

//    @MapsId("ID_LOJA")
//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO")
//    private Usuario usuario;

    @Column(name = "NO_LOJA")
    private String nome;

}
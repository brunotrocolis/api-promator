package com.trocolis.apipromator.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CAD_CLIENTE")
public class Cliente {

    @Id
    @Column(name = "ID_CLIENTE")
    private Long id;

    @MapsId
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CLIENTE")
    private Usuario usuario;

    @Column(name = "NO_CLIENTE")
    private String nome;

}

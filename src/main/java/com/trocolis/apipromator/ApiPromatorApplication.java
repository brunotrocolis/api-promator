package com.trocolis.apipromator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPromatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPromatorApplication.class, args);
	}

}

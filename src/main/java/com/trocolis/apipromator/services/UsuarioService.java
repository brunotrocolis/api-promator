package com.trocolis.apipromator.services;

import com.trocolis.apipromator.exception.SenhaInvalidaException;
import com.trocolis.apipromator.exception.UsuarioInvalidoException;
import com.trocolis.apipromator.model.dto.CredenciaisDTO;
import com.trocolis.apipromator.model.dto.TokenDTO;
import com.trocolis.apipromator.model.dto.UsuarioDTO;
import com.trocolis.apipromator.model.entity.Usuario;
import com.trocolis.apipromator.model.enums.SituacaoUsuario;
import com.trocolis.apipromator.repository.UsuarioRepository;
import com.trocolis.apipromator.security.jwt.JwtService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class UsuarioService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private JwtService jwtService;

    public TokenDTO getTokenDTO(CredenciaisDTO credenciaisDTO) {
        try {
            UsuarioDTO usuarioDTO = usuarioRepository.getUsuarioLoginDTO(credenciaisDTO.getUsername());
            validaSenha(credenciaisDTO.getPassword(), usuarioDTO.getPassword());
            return new TokenDTO(usuarioDTO.getUsername(), jwtService.getToken(usuarioDTO.getUserDetails()));
        } catch (NullPointerException e) {
            throw new UsuarioInvalidoException();
        }
    }

    private void validaSenha(String requestPassword, String userPassword) {
        if(!passwordEncoder.matches(requestPassword, userPassword))
            throw new SenhaInvalidaException();
    }

    public UserDetails autenticarUsuario(CredenciaisDTO credenciais) {
        UserDetails userDetails = loadUserByUsername(credenciais.getUsername());
        boolean senhaCorreta = passwordEncoder.matches(credenciais.getPassword(), userDetails.getPassword());
        if (senhaCorreta) {
            return userDetails;
        }
        throw new SenhaInvalidaException();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            UsuarioDTO usuario = usuarioRepository.getUsuarioLoginDTO(username);
            return usuario.getUserDetails();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            throw new UsernameNotFoundException("Usuário não encontrado");
        }
    }

    public void criarUsuario(UsuarioDTO usuarioDTO) {
        Usuario usuario = new Usuario();

        usuario.setNome(usuarioDTO.getUsername());
        usuario.setSenha(passwordEncoder.encode(usuarioDTO.getPassword()));
        usuario.setTipo(usuarioDTO.getRole());
        usuario.setSituacao(SituacaoUsuario.INATIVO);

        usuarioRepository.save(usuario);
    }
}

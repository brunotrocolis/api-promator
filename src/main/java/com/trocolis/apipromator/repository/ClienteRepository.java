package com.trocolis.apipromator.repository;

import com.trocolis.apipromator.model.entity.Cliente;
import com.trocolis.apipromator.model.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}

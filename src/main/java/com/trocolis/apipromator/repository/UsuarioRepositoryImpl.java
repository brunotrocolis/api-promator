package com.trocolis.apipromator.repository;

import com.trocolis.apipromator.model.dto.UsuarioDTO;
import com.trocolis.apipromator.model.entity.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Repository
public class UsuarioRepositoryImpl extends JpaRepository{

    @Value("${source.sql.usuario.get-usuario-dto}")
    private String SQL_GET_USUARIO_DTO;

    @Cacheable("user")
    public UsuarioDTO getUsuarioDTO(String username) {
        return createQuery(SQL_GET_USUARIO_DTO, UsuarioDTO.class)
                .setParameter("username", username)
                .getSingleResult();
    }

}

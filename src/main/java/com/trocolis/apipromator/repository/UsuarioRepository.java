package com.trocolis.apipromator.repository;

import com.trocolis.apipromator.model.dto.UsuarioDTO;
import com.trocolis.apipromator.model.entity.Usuario;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findByNome(String nome);

    @Cacheable("user")
    @Query("SELECT new com.trocolis.apipromator.model.dto.UsuarioDTO(nome, senha, tipo) FROM Usuario WHERE nome = :username")
    UsuarioDTO getUsuarioLoginDTO(String username);
}

package com.trocolis.apipromator.controller;


import com.trocolis.apipromator.exception.SenhaInvalidaException;
import com.trocolis.apipromator.model.dto.CredenciaisDTO;
import com.trocolis.apipromator.model.dto.TokenDTO;
import com.trocolis.apipromator.model.dto.UsuarioDTO;
import com.trocolis.apipromator.model.enums.TipoUsuario;
import com.trocolis.apipromator.security.jwt.JwtService;
import com.trocolis.apipromator.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private JwtService jwtService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void criarCliente(@RequestBody UsuarioDTO usuarioDTO) {
        try {
            usuarioDTO.setRole(TipoUsuario.CLIENTE);
            usuarioService.criarUsuario(usuarioDTO);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("auth")
    public TokenDTO autenticar(@RequestBody CredenciaisDTO credenciais) {
        try {
            UserDetails userDetails = usuarioService.autenticarUsuario(credenciais);
            return new TokenDTO(
                    userDetails.getUsername(),
                    jwtService.getToken(userDetails));
        } catch (UsernameNotFoundException | SenhaInvalidaException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

}

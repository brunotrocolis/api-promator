package com.trocolis.apipromator.controller;

import com.trocolis.apipromator.model.entity.Cliente;
import com.trocolis.apipromator.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("cliente")
public class ClienteController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping
    public List<Cliente> listarClientes() {
        return clienteRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente inserirCliente(@RequestBody Cliente cliente) {
        cliente.getUsuario().setSenha(
                passwordEncoder.encode(cliente.getUsuario().getSenha())
        );
        return clienteRepository.save(cliente);
    }
}

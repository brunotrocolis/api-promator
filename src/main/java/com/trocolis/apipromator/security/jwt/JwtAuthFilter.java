package com.trocolis.apipromator.security.jwt;

import com.trocolis.apipromator.exception.TokenInvalidoException;
import com.trocolis.apipromator.services.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthFilter extends OncePerRequestFilter {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UsuarioService usuarioService;

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorization = request.getHeader("Authorization");
        try {
            validarAuthorization(authorization);

            String token = authorization.split(" ")[1];
            String loginUsuario = jwtService.getUsername(token);
            UserDetails usuario = usuarioService.loadUserByUsername(loginUsuario);
            UsernamePasswordAuthenticationToken user = new
                    UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
            user.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(user);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            filterChain.doFilter(request, response);
        }
    }

    private void validarAuthorization(String authorization) {
        if (authorization == null
                || authorization.isEmpty()
                || authorization.isBlank()
                || !authorization.startsWith("Bearer"))
            throw new TokenInvalidoException();
    }
}

package com.trocolis.apipromator.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Service
public class JwtService {

    @Value("${security.jwt.expiration}")
    private String expiration;

    @Value("${security.jwt.signing-key}")
    private String signingKey;

    public String getToken(UserDetails userDetails) {
        GregorianCalendar dateExpiration = new GregorianCalendar();
        dateExpiration.setTime(new Date());
        dateExpiration.add(Calendar.MINUTE, Integer.parseInt(expiration));

        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setExpiration(dateExpiration.getTime())
                .signWith(SignatureAlgorithm.HS512, signingKey)
                .compact();
    }

    private Claims getClaims(String token) {
        return Jwts.parser()
                .setSigningKey(signingKey)
                .parseClaimsJws(token)
                .getBody();
    }

    public String getUsername(String token) {
        return (String) getClaims(token).getSubject();
    }
}

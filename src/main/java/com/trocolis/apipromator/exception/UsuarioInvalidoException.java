package com.trocolis.apipromator.exception;

public class UsuarioInvalidoException extends RuntimeException {

    public UsuarioInvalidoException() {
        super("Usuaário Inválido!");
    }
}

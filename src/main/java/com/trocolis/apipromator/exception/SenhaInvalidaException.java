package com.trocolis.apipromator.exception;

public class SenhaInvalidaException extends RuntimeException {

    public  SenhaInvalidaException() {
        super("Senha Inválida!");
    }
}

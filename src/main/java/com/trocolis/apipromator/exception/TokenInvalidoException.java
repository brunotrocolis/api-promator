package com.trocolis.apipromator.exception;

public class TokenInvalidoException extends RuntimeException {
    public TokenInvalidoException() {
        super("Token Inválido!");
    }
}

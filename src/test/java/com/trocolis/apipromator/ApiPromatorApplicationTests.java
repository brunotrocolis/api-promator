package com.trocolis.apipromator;

import com.trocolis.apipromator.model.entity.Cliente;
import com.trocolis.apipromator.model.entity.Usuario;
import com.trocolis.apipromator.model.enums.TipoUsuario;
import com.trocolis.apipromator.repository.ClienteRepository;
import com.trocolis.apipromator.repository.UsuarioRepository;
import com.trocolis.apipromator.services.EmailService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import javax.mail.MessagingException;
import java.util.Optional;

@SpringBootTest
class ApiPromatorApplicationTests {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private EmailService emailService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiPromatorApplicationTests.class);

    @Test
    void criarUsuarioCliente() {
        LOGGER.info("Iniciando o teste de cadastro de usuário");
        Usuario usuario = new Usuario();
        usuario.setNome("bruno");
        usuario.setSenha("$2y$12$Bm.uiUZXJWxVv6Su5TYA3e4M2HnPpbS.v6CmTxatsdleTahesBI8m");
        usuario.setTipo(TipoUsuario.ADMIN);

        Cliente cliente = new Cliente();
        cliente.setUsuario(usuario);
        cliente.setNome("Bruno Trócolis");

        try {
            clienteRepository.save(cliente);
            LOGGER.info("Teste de cadastro de usuário finalizado");
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Usuário já cadastrado, executar teste deletarUsuarioCliente()");
        }

    }

    @Test
    void deletarUsuarioCliente() {
        LOGGER.info("Iniciando o teste de exclusão de usuário");
        Optional<Usuario> usuario = usuarioRepository.findByNome("bruno");
        if (usuario.isPresent()) {
            clienteRepository.deleteById(usuario.get().getId());
            LOGGER.info("Teste de exclusão de usuário finalizado");
        } else
            LOGGER.warn("Usuário teste não cadastrado, executar teste criarUsuarioCliente()");
    }

//    @Test
//    void enviarEmail() throws MessagingException {
//        emailService.enviarEmail();
//    }

}
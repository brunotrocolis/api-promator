package com.trocolis.apipromator;

import com.trocolis.apipromator.security.jwt.JwtService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

@SpringBootTest
public class JwtServiceTest {

    @Autowired
    private JwtService jwtService;

    @Test
    public void testarGetToken() {

        UserDetails userDetails = User.builder()
                .username("bruno")
                .password("1234")
                .roles("ADMIN")
                .build();

        String token = jwtService.getToken(userDetails);

        Assert.isInstanceOf(String.class, token);
    }
}
